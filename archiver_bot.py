#!/usr/bin/python3
from telegram.ext import Application
from telegram     import Update
from telegram.ext import CallbackContext
from telegram.ext import MessageHandler, filters
from telegram     import constants
import json
from archiver_utilities import download_url_by_type, center_message, clean_title

channels = {}
credentials_file = 'confidentials.json'
config_file      = 'config.json'
MESSAGE_LENGTH   = 55

import logging

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.INFO)

from datetime     import datetime
import asyncio
#from tweetcapture import TweetCapture

MAX_DOWNLOAD_FILESIZE = constants.FileSizeLimit.FILESIZE_DOWNLOAD

def get_current_date():
    currentdate = str(datetime.now())
    currentdate = currentdate.replace(' ','_')
    return currentdate

async def telegramdownload(update,context,file_id,file_name):
    """
    Download messages when they are a document or a photo.
    """
    global channels

    message_id = update.channel_post.message_id
    
    if (channel_id := str(update.channel_post.chat.id)) in channels:
        print(center_message(channels[channel_id]['name'] + ' Channel',MESSAGE_LENGTH,'='))
        newFile = await context.bot.get_file(file_id)
        await newFile.download_to_drive(custom_path=(channels[channel_id]['path'] + file_name))
        await context.bot.delete_message(int(channel_id),message_id)
        print('| ' + file_name + ' saved.')
        print(center_message('Message Deleted',MESSAGE_LENGTH,'-'))
    else:
        print('Bot added to another channel')
        print(update)

async def photohandler(update: Update, context: CallbackContext):
    #The last photo is the largest one.
    photo     = update.channel_post.photo[-1]
    file_id   = photo.file_id
    width     = photo.width
    
    if update.channel_post.forward_from and update.channel_post.forward_from.username:
        file_name = update.channel_post.forward_from.username
    elif update.channel_post.caption:
        file_name = clean_title(update.channel_post.caption)[:250]
    else:
        file_name = 'photo'
    file_name += '_' + get_current_date() + '.jpg'
    await telegramdownload(update,context,file_id,file_name)

async def videohandler(update: Update, context: CallbackContext):
    video = update.channel_post.video
    if video.width:
        file_id = video.file_id
        width   = video.width
    if update.channel_post.forward_from and update.channel_post.forward_from.username:
        file_name = update.channel_post.forward_from.username
    elif update.channel_post.caption:
        file_name = clean_title(update.channel_post.caption)[:250]
    else:
        file_name = 'video'
    file_name += '_' + get_current_date() + '.mp4'
    await telegramdownload(update,context,file_id,file_name)
    

async def allhandler(update: Update, context: CallbackContext):
    print(update)

async def documenthandler(update: Update, context: CallbackContext):
    file_id   = update.channel_post.document.file_id
    file_name = update.channel_post.document.file_name
    if (file_size := update.channel_post.document.file_size and
        update.channel_post.document.file_size > MAX_DOWNLOAD_FILESIZE):
        print('File has size bigger than 20MB. Size: ' + str(file_size))
        print('File needs to be downloaded manually.')
        return
    await telegramdownload(update,context,file_id,file_name)

async def urlhandler(update: Update, context: CallbackContext):
    """
    What should the bot do when a message is received. We delete the message
    afterwards.
    """
    global channels
    message_id = update.channel_post.message_id
    url = update.channel_post.text
    if (channel_id := str(update.channel_post.chat.id)) in channels:
        print(center_message(channels[channel_id]['name'] + ' Channel',MESSAGE_LENGTH,'='))
        result = download_url_by_type(url,channels[channel_id]['path'])
        if result[0]:
            await context.bot.delete_message(int(channel_id),message_id)
            print(center_message('Message Deleted',MESSAGE_LENGTH,'-'))
        else:
            await context.bot.send_message(int(channel_id),result[1])
            print(center_message('Error Message Replied',MESSAGE_LENGTH,'-'))
    else:
        print('Bot added to another channel')
        print(update)

    print()

def get_channels(json_file):
    """
    From the config file get all the channels defined and check that all have the correct values defined.
    """
    with open(json_file) as f:
        channels = json.loads(f.read())
        for key in channels.keys():
            if not channels[key]["name"] or not channels[key]["path"]:
                raise Exception("This channel has some values missing. Channel" + str(channel))

    return channels


def get_credentials(json_file):
    """
    From the credentials file get the tokens and cookies
    """
    with open(json_file) as f:
        confidentials = json.loads(f.read())
        if "api_key" not in confidentials or "api_secret" not in confidentials:
            raise e.ConfidentialsNotSuppliedError()

        telegram_token = confidentials["telegram_token"]
    return telegram_token

def main():
    """
    Run the bot
    """
    global channels
    #Get the channels where the bot will operate from the json file. 
    channels = get_channels(config_file)
    #Get the token that the bot needs to be able to work with Telegram.
    telegram_token = get_credentials(credentials_file)
    
    #Initialize the bot
    application = Application.builder().token(telegram_token).build()

    #Add the functions depending on the type of message.
    application.add_handler(MessageHandler(filters.TEXT & filters.Entity('url'), urlhandler))
    application.add_handler(MessageHandler(filters.PHOTO       , photohandler))
    application.add_handler(MessageHandler(filters.VIDEO       , videohandler))
    application.add_handler(MessageHandler(filters.Document.ALL, documenthandler))
    application.add_handler(MessageHandler(filters.ALL, allhandler))

    #Start the bot
    application.run_polling()


if __name__ == '__main__':
    main()
