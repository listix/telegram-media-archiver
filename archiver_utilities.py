#!/usr/bin/python3
import requests
import json
import re
import os
import shutil
import math
import tldextract
from subprocess import call
from yt_dlp import YoutubeDL
from yt_dlp.utils import DownloadError, ExtractorError
from bs4 import BeautifulSoup
from urllib.request import Request, urlopen
from datetime import datetime

import sys
sys.path.insert(0,'deviantart_downloader')
import frontend
import explorer

import asyncio

#import tweepy

api_key = ""
api_secret = ""
access_token = ""
access_token_secret = ""
bearer_token = ""
fa_cookie = ""
da_user = ""
credentials_file = 'confidentials.json'
TWITTER_API = 'https://api.twitter.com/2/tweets/'
REDDIT_API = 'https://api.reddit.com/api/info/?id=t3_'
MSGSIZE = 55

renameresultda = True
errormsgda = ""

ytprefixes = ["https://www.youtube","https://youtu.be","https://youtube.com","https://m.youtube"]

def bearer_oauth(r):
    """
    Method required by bearer token authentication.
    """

    r.headers["Authorization"] = f"Bearer {bearer_token}"
    r.headers["User-Agent"] = "v2TweetLookupPython"
    return r

def connect_to_endpoint(url):
    """
    Connect to the twitter api and get the response as a json.
    """
    response = requests.request("GET", url, auth=bearer_oauth)
    if response.status_code != 200:
        raise Exception(
            "Request returned an error: {} {}".format(
                response.status_code, response.text
            )
        )
    return response.json()

def download_image(image, path, timestamp, size, cookie):
    """
    Download and save image to path.
    Args:
        image: The url of the image.
        path: The directory where the image will be saved.
        timestamp: The time that the image was uploaded.
            It is used for naming the image.
        size: Which size of images to download.
    """
    count = 0
    def print_status(s):
        import sys

        sys.stdout.write("\u001b[1K")
        spinner = ["-", "\\", "|", "/"][count % 4]
        print(f"\r{spinner} {s}")

    result = True
    errormsg = ""
    if image:
        # image's path with a new name
        filename = os.path.basename(image)
        timestamp = timestamp[0:(255-len(filename)-1)]
        name = timestamp + filename
        save_dest = os.path.join(path, name)
        print(save_dest)
        # save the image in the specified directory if
        if not (os.path.exists(save_dest)):
            if cookie:
                headers = {'Cookie':cookie}
                r = requests.get(image + size, stream=True, headers=headers)
            else:
                r = requests.get(image + size, stream=True)
            if r.status_code == 200:
                with open(save_dest, "wb") as f:
                    r.raw.decode_content = True
                    shutil.copyfileobj(r.raw, f)
                count += 1
                print_status(f"{name} saved")
                print()
            else:
                errormsg = "File unable to be downloaded reason: {}".format(r.status_code)
                result = False
        else:
            print_status(f"Skipping {name}: already downloaded")

    return (result,errormsg)

def extract_tweet_medias_old(tweet):
    """
    Return a list of url(s) which represents the image(s) embedded in tweet.
    """
    medias = []
    if 'includes' in tweet and 'media' in tweet['includes']:
        medias = [x for x in tweet['includes']['media']]
    else:
        print('----')
        print(tweet)
        print('----')
    return medias

def extract_tweet_medias(tweet):
    """
    Return a list of url(s) which represents the image(s) embedded in tweet.
    """
    medias_json = tweet.extended_entities.get("media",[])
    return medias_json
    
def get_credentials(json_file):
    """
    From the credentials file get the tokens and cookies
    """
    global api_key
    global api_secret
    global access_token
    global access_token_secret
    global bearer_token
    global fa_cookie
    global da_user
    with open(json_file) as f:
        confidentials = json.loads(f.read())
        if "api_key" not in confidentials or "api_secret" not in confidentials:
            raise e.ConfidentialsNotSuppliedError()

        api_key      = confidentials["api_key"]
        api_secret   = confidentials["api_secret"]
        bearer_token = confidentials["bearer_token"]
        fa_cookie    = confidentials["fa_cookie"]
        da_user      = confidentials["da_user"]
        access_token = confidentials["access_token"]
        access_token_secret = confidentials["access_token_secret"]

def extract_twitter_user_id(tweet):
    """
    Get the username and tweet id from an arbitrary tweet.
    """
    result = re.search(r"https://(mobile.)?twitter.com/([^/]+)/status/([0-9]+)",tweet)
    #       username        tweet id
    return (result.group(2),result.group(3))



def get_tweet_json_old(tweet):
    """
    Get the json of a tweet that contains the media fields in order to get the 
    image urls.
    """
    tweet_user_id = extract_twitter_user_id(tweet)
    url = TWITTER_API + str(tweet_user_id[1]) + '?media.fields=url&expansions=attachments.media_keys'
    response = connect_to_endpoint(url)
    return (tweet_user_id[0],tweet_user_id[1],response)

def get_tweet_json(tweet):
    tweet_user_id = extract_twitter_user_id(tweet)
    #auth = tweepy.OAuth1UserHandler(
    #    api_key, api_secret, access_token, access_token_secret
    #    )
    #api = tweepy.API(auth)
    #tweet_data = api.get_status(id=tweet_user_id[1],include_entities=True,tweet_mode="extended")
    #return (tweet_user_id[0],tweet_user_id[1],tweet_data)
    

def clean_title(title):
    title = title.replace(' ','_')
    title = re.sub('[\W]+','',title)
    title = re.sub(r'[^\x00-\x7F]+','_', title)
    return title 


class loggerOutputs:
    def error(msg):
        print("Captured Error: "+msg)
    def warning(msg):
        print("Captured Warning: "+msg)
    def debug(msg):
        print("Captured Log: "+msg)

def remove_ansi_color(text):
    # 7-bit C1 ANSI sequences
    ansi_escape = re.compile(r'''
        \x1B  # ESC
        (?:   # 7-bit C1 Fe (except CSI)
            [@-Z\\-_]
        |     # or [ for CSI, followed by a control sequence
            \[
            [0-?]*  # Parameter bytes
            [ -/]*  # Intermediate bytes
            [@-~]   # Final byte
        )
    ''', re.VERBOSE)
    result = ansi_escape.sub('', text)
    return result

def yt_dl(url,path):
    """
    Function for using ytp-dl. We make sure to get the title from the download
    and add it to the filename.
    """
    ydl_opts = {'forcetitle':True,
                'logger':loggerOutputs}

    result = ""
    with YoutubeDL(ydl_opts) as ydl:
        try:
            result = ydl.extract_info(url,download=False)
            if result == 0:
                result = [True,""]
        except ExtractorError as exmsg:
            return [False,"Extractor Error: " + remove_ansi_color(str(exmsg))]
        except DownloadError as exmsg:
            return [False,"Download Error: " + remove_ansi_color(str(exmsg))]

    title = result['title']
    dash_pos = title.find("-")
    title = title[dash_pos+2:dash_pos+101]
    title = clean_title(title)
    ydl_opts = {'outtmpl': path + "%(uploader_id)s_%(upload_date)s_%(id)s_" + title + ".%(ext)s",
                'logger':loggerOutputs}
    result = ""
    with YoutubeDL(ydl_opts) as ydl:
        try:
            result = ydl.download([url])
            if result == 0:
                result = [True,""]
        except ExtractorError as exmsg:
            result = [False,"Extractor Error: " + remove_ansi_color(str(exmsg))]
        except DownloadError as exmsg:
            result = [False,"Download Error: " + remove_ansi_color(str(exmsg))]
    return result

def download_fa_file(url,path):
    """
    This will get the page from FA using the cookie and find the download link.
    Then it will proceed to download the file as normal.
    """
    file_link = ""
    if not url.startswith("https://d.furaffinity.net"):
        request = Request(url)
        request.add_header('Cookie',fa_cookie)
        html_page = urlopen(request)
        soup = BeautifulSoup(html_page,features="html.parser")
        links = soup.findAll('a',attrs={'href':re.compile(r"^//d.furaffinity.net")})
        if len(links) == 0:
            return [False,"Invalid FA link. No links found."]
        file_link = 'https:' + links[0].get('href')
    else:
        file_link = url
    result = download_image(file_link, path, '', '', fa_cookie)
    return result

def get_deviation_id(url):
    """
    Given a DA url obtain the deviation_id
    """
    request = Request(url)
    #request.add_header('Cookie',fa_cookie)
    html_page = urlopen(request)
    soup = BeautifulSoup(html_page,features="html.parser")
    links = soup.findAll('meta',attrs={'content':re.compile(r"^DeviantArt://deviation/")})
    da_appurl = links[0].get('content')
    result = re.search(r"DeviantArt://deviation/([-A-F0-9]+)",da_appurl)
    return result.group(1)

def get_deviation_object(daexplorer,deviation_id):
    return daexplorer._api("/deviation/"+deviation_id)

import glob, os, time

def rename_da_file(author,title,path,deviationid):
    currentdate = str(datetime.now())
    currentdate = currentdate.replace(' ','_')
    global renameresultda
    renameresultda = False
    global errormsgda
    errormsgda = "Error could not rename file: " + path + deviationid
    newfilename = ""

    for file in glob.glob(path + deviationid + "*"):
        ext = os.path.splitext(file)[1]
        newfilename = "{}_{}_{}".format(author,currentdate,title)
        newfilename = path + newfilename[0:250] + ext
        os.rename(file, newfilename)
        renameresultda = True
        errormsgda = ""
    basefilename = os.path.basename(newfilename)
    print('\rDownloading DA file: {}\r'.format(basefilename))
    return [renameresultda,errormsgda]

def download_da_deviation(url,path):
    """
    Download a deviation using the Deviantart download package.
    """
    downloader = frontend.DAFrontend()
    downloader.creds = explorer.Credentials().from_file(credentials_file)
    deviationid = get_deviation_id(url)

    downloader.api = explorer.DAExplorer(credentials = downloader.creds,target_user = da_user)
    response = get_deviation_object(downloader.api,deviationid)
    deviation = explorer.Deviation()
    deviation.deviationid = response["deviationid"]
    deviation.is_downloadable = response["is_downloadable"]
    if "videos" in response:
        bestquality = 0
        for video in response["videos"]:
            for quality in ['1080p','720p','480p','360p']:
                if "quality" in video and quality in video['quality']:
                    if bestquality < int(quality[0:-1]):
                        bestquality = int(quality[0:-1])
                        deviation.preview_src = video["src"]
    elif "content" in response:
        deviation.preview_src = response["content"]["src"]
    author = response["author"]["username"]

    title = clean_title(response["title"])
    try:
        loop = asyncio.get_running_loop()
    except RuntimeError:  # 'RuntimeError: There is no current event loop...'
        loop = None
    
    if loop and loop.is_running():
        print('Async event loop already running. Adding coroutine to the event loop.')
        tsk = loop.create_task(downloader.api.download_deviation(deviation,path))
        # ^-- https://docs.python.org/3/library/asyncio-task.html#task-object
        # Optionally, a callback function can be executed when the coroutine completes
        tsk.add_done_callback(lambda t: rename_da_file(author,title,path,deviationid))
        #lambda t: print(f'Task done with result={t.result()}  << return val of main()'))
    else:
        print('Starting new event loop')
        result = asyncio.run(downloader.api.download_deviation(deviation,path))
    #asyncio.run(downloader.api.download_deviation(deviation,path))
    

    global renameresultda
    global errormsgda 

    return [renameresultda,errormsgda]



def download_twitter_video(username,tweet_id,url,path):
    """
    We use ytp-dl to download the twitter video.
    """
    print('\rDownloading twitter video: {}\r'.format(url))
    result = yt_dl(url,path)
    return result

def download_twitter_image(username,tweet_id,url,path):
    """
    When downloading a twitter image we want to keep the username of the account
    and the tweet id so those are prepended to the filename.
    """
    result = download_image(url, path, '{}_{}_'.format(username,tweet_id), ':large','')
    return result

def download_youtube_video(url,path):
    """
    This function downloads a youtube video using ytp-dl.
    """
    result = yt_dl(url,path)
    return result

def connect_to_reddit_endpoint(url):
    """
    Connect to the reddit api and get the response as a json.
    """
    response = requests.request("GET", url, headers = {'User-agent': 'telegram_archiver 0.1'})
    if response.status_code != 200:
        raise Exception(
            "Request returned an error: {} {}".format(
                response.status_code, response.text
            )
        )
    return response.json()

def extract_reddit_post_id(post):
    """
    Get the username and tweet id from an arbitrary tweet.
    """
#https://old.reddit.com/r/redditdev/comments/c93rdt/how_do_i_get_json_data_of_a_specific_post_without/
    result = re.search(r"https:\/\/(old\.|www\.)?reddit.com\/r\/([0-9a-zA-Z_-]+)\/comments\/([0-9a-zA-Z]+)\/.*",post)
    #      post_id 
    return result.group(3)

def get_reddit_data(url):
    postid = extract_reddit_post_id(url)
    url = REDDIT_API + str(postid)
    response = connect_to_reddit_endpoint(url)
    title = ""
    author = ""
    mediaurl = []
    mediatype = ""
    if 'data' in response and 'children' in  response['data']:
        children = response['data']['children']
        for child in children:
            if 'data' in child:
                title = child['data']['title'] if 'title' in child['data'] else 'NO_TITLE'
                author = child['data']['author'] if 'author' in child['data'] else 'NO_AUTHOR'
                if 'is_video' in child['data']:
                    if not child['data']['is_video'] and 'is_gallery' in child['data']:
                        for img_id in child['data']['media_metadata'].keys():
                            mime_type = re.search(r"image\/([0-9a-zA-Z]+)",child['data']['media_metadata'][img_id]['m'])
                            mime_type = mime_type.group(1)
                            mediaurl.append('https://i.redd.it/{}.{}'.format(img_id,mime_type))
                        mediatype = "image"
                    elif not child['data']['is_video'] and 'url' in child['data']:
                        mediaurl = [child['data']['url']]
                        mediatype = "image"
                    elif child['data']['is_video'] and 'media' in child['data']:
                        mediaurl.append(child['data']['media']['reddit_video']['fallback_url'])
                        mediatype = "video"
                    else:
                        mediatype = "other"
                        print('Other')

    return (mediaurl,mediatype,author,postid,title)


def download_reddit_media(url,path):
    mediaurls,mediatype,author,postid,title = get_reddit_data(url)
    title = clean_title(title)
    result = [True,""]
    if mediatype in ['image']:
        currentdate = str(datetime.now())
        currentdate = currentdate.replace(' ','_')
        downloadresult = []
        for mediaurl in mediaurls:
            downloadresult = download_image(mediaurl,path,'{}_{}_{}_{}_'.format(author,currentdate,postid,title),'','')
            result[0] = result[0] and downloadresult[0]
            result[1] = result[1] + ', ' + downloadresult[1]
    elif mediatype in ['video']:
        result = yt_dl(url,path)
    elif mediatype in ['other']:
        result = [False, 'Media type: Other. Not supported']
    else:
        result = [False, 'No data found. Check if the URL is correct.']

    return result

def download_file(url,path):
    """
    This will download a file and keep the filename.
    """
    result = download_image(url, path, '', '','')
    return result

def download_discord_file(url,path):
    """
    This will use a discord url and download the file. Many times the file is 
    called unknown.png so this will append the current date to the name before
    saving it.
    """
    currentdate = str(datetime.now())
    currentdate = currentdate.replace(' ','_')
    result = download_image(url, path, currentdate + '_', '','')
    return result

def download_tweet(url,path):
    """
    This function will download either video or images from a tweet.
    """
    username,tweet_id,medias_json = get_tweet_json(url)
    medias = extract_tweet_medias(medias_json)
    twitter_video_urls = []
    twitter_image_urls = []

    for media in medias:
        match = re.search(r"/video/",media["expanded_url"])
        if match is not None:
            twitter_video_urls.append(url)
        elif 'type' in media:
            if media['type'] in ['video','animated_gif']:
                if 'media_url_https' in media:
                    twitter_video_urls.append(media['media_url_https'])

                else:
                    twitter_video_urls.append(url)

            elif media['type'] == 'photo':
                twitter_image_urls.append(media['media_url_https'])

            else:
                print('Media not supported: {}'.format(media))
        elif medias_json.full_text:
            texturl = re.search(r"https://t.co/[a-zA-Z0-9]+",medias_json.full_text)
            if texturl.startswith("https://t.co/"):
                twitter_video_urls.append(url)

    if medias == [] and medias_json.full_text:
        match = re.search(r"https://t.co/[a-zA-Z0-9]+",medias_json.full_text)
        if match is not None and match.group(0).startswith("https://t.co/"):
            twitter_video_urls.append(url)

    result = [True,""]
    downloadresult = []

    for video_url in twitter_video_urls:
        downloadresult = download_twitter_video(username,tweet_id,video_url,path)
        result[0] = result[0] and downloadresult[0]
        result[1] += ', ' + downloadresult[1]
    for image_url in twitter_image_urls:
        downloadresult = download_twitter_image(username,tweet_id,image_url,path)
        result[0] = result[0] and downloadresult[0]
        result[1] += ', ' + downloadresult[1]

    return result


def center_message(text,size,padding):
    return '+{}+'.format((' {} '.format(text)).center(size,padding))

def call_download_and_msg(function,site,url,path):
        print(center_message(site,MSGSIZE,'-'))
        result = function(url,path)
        status = ' Complete' if result[0] else ' Error'
        print(center_message(site + status,MSGSIZE,'-'))
        return result

def download_url_by_type(url,path):
    """
    Principal function that this package provides. It will check the
    url and run the appropriate function to download the content.
    """
    get_credentials(credentials_file)
    
    redirect = requests.get(url)
    url = redirect.url
    
    domain = tldextract.extract(url).domain
    result = []

    if domain.endswith("youtube") or domain == "youtu":
        result = call_download_and_msg(download_youtube_video,'Youtube',url,path)

    elif domain.endswith("twitter") or domain == "x":
        url = url.replace(domain,'twitter')
        #result = call_download_and_msg(download_tweet,'Twitter',url,path)
        result = (False,"Not downloading twitter for now so no deleting the message.")

    elif domain.endswith("furaffinity"):
        url = url.replace(domain,'furaffinity')
        result = call_download_and_msg(download_fa_file,'FA',url,path)

    elif url.startswith("https://cdn.discordapp.com") and (url.endswith("unknown.png") or url.endswith("image.png")):
        result = call_download_and_msg(download_discord_file,'Discord',url,path)

    elif domain.endswith("deviantart") or url.startswith("https://sta.sh"):
        url = url.replace(domain,'deviantart')
        result = call_download_and_msg(download_da_deviation,'Deviantart',url,path)

    elif domain == "reddit":
        result = call_download_and_msg(download_reddit_media,'Reddit',url,path)

    else:
        result = call_download_and_msg(download_file,'Other',url,path)

    return result
