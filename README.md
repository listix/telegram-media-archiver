# Telegram Media Archiver


## Installation
1. Clone this repo
2. ```pip install -r requirements.txt```
3. If you want support for deviantart enter the directory and clone this repository [deviantart\_downloader](https://github.com/merberich/deviantart_downloader)

## Setup

You need to fill the information in confidentials.json.

For twitter you need to generate a [Twitter bearer token](https://developer.twitter.com/en/docs/authentication/oauth-2-0). Those values need to be entered here:

```
  "api_key": "",
  "api_secret": "",
  "bearer_token": "",
```

This is the literal FA cookie, you need to view the cookies when logged in to FA

```
  "fa_cookie": "",
```

You need to create a token for the [Telegram API](https://core.telegram.org/bots#6-botfather). This allows you to create the bot.

```
  "telegram_token": "",
```

If you want deviantart support you need to create an [app](https://www.deviantart.com/developers/apps). The user is your username.

```
  "client_id":"",
  "client_secret":"",
  "da_user":""
```

Once you have created the Telegram bot using the botfather you need to create a Channel and invite your newly created bot to it. The only permissions the bot should have is "Post messages" and "Delete messages of others". 

To do the final step of setup you need to run the bot to get some information.

```
python archive_bot.py
```

The program should start and then you should send a message to the newly created channel. On the terminal you will see a notification of that message that the bot just saw. You are interested in the channel id(e.g -123456789). This process needs to be repeated for all the channels that you are creating.

You also need to create directories where the media will be saved. You can create one directory per channel.

Now you can fill config.json

```
{"-123456789": {"name":"Write a title for the channel",
                "path":"The directory you just created"
                 },
```

Note: Be sure to put the whole path of the directory.

Once you have all the channels created and the config.json filled with the information, close the bot with ctrl+c and restart it.


```
python archiver_bot.py
```

Now the bot will listen to the messages in the chat. Whenever a link is posted to it the bot will download the media and save it to the folder specified for that Channel.

## License
GNU General Public License v3.0

